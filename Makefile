.PHONY: build clean upload bump justbump

build:
	python3 -m build

check:
	pylint setup.py multi-git

clean:
	rm -rf dist/ build/ multi_git.egg-info/

upload: clean build
	twine upload -u danielkullmann dist/*

justbump-patch:
	bump2version patch

bump-patch: justbump-patch upload

justbump-minor:
	bump2version minor

bump-minor: justbump-minor upload

justbump-major:
	bump2version major

bump-major: justbump-major upload

